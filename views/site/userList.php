<?php
    use app\models\User;
    use yii\helpers\Url;

?>




<table>
    <?php foreach ($userList as $i => $model): ?>
        <tr>
            <td>----Name: <?=$userList[$i]->username?></td>
            <td>----Email: <?=$userList[$i]->email?></td>
            <td>----Deactivate: <?=$userList[$i]->status?></td>
            <td>

                <?php
                if ($userList[$i]->status) {
                echo '<a class="btn btn-primary" href="' . Url::to(['/site/up-status', 'id' => $userList[$i]->id, 'status' => User::STATUS_ACTIVE]) . '"' . '>Activate' . '</a>';
                } else {
                echo '<a class="btn btn-danger" href="' . Url::to(['/site/up-status', 'id' => $userList[$i]->id, 'status' => User::STATUS_DELETED]) . '"' . '>Deactivate' . '</a>';
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>